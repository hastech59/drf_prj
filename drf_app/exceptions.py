from rest_framework.views import exception_handler
from .status import *
from rest_framework.response import Response
from rest_framework import status

def handle_exception(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)
    StatusObj = StatusClass()

    # # Now add the HTTP status code to the response.
    if response is not None:
        JsonResponse = StatusObj.status_other(
               message = response.status_text,
               status_code = response.status_code
           )
        return Response(JsonResponse, status=response.status_code)
    else:
        JsonResponse = StatusObj.status_400(data = str(exc))
        return Response(JsonResponse,status=status.HTTP_400_BAD_REQUEST)

    return response
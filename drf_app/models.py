from django.db import models
from .managers import *

class UsersModal(models.Model):
    u_id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=20)
    password = models.CharField(max_length=20)
    f_name = models.CharField(max_length=200)
    l_name = models.CharField(max_length=200)
    age = models.IntegerField(null=True)
    street = models.CharField(max_length=100)
    pin_code = models.CharField(max_length=10)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    country = models.CharField(max_length=100)

    USERNAME_FIELD = 'u_id'
    REQUIRED_FIELDS = []
    # objects = MyUserManager()
    # REQUIRED_FIELDS = ['u_id','password','password','f_name','l_name','age','street','pin_code','city','state','country']

    @property
    def is_anonymous(self):
        return True
    
    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    def __str__(self):
        return str(self.u_id)

class AdsModal(models.Model):
    u_id = models.ForeignKey(UsersModal,on_delete=models.CASCADE)
    a_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=200)
    price_per_km = models.CharField(max_length=10)

    def __str__(self):
        return str(self.a_id)

class CarsModal(models.Model):
    u_id = models.ForeignKey(UsersModal,on_delete=models.CASCADE)
    c_id = models.AutoField(primary_key=True)
    brand = models.CharField(max_length=100)
    model = models.CharField(max_length=200)
    number_plate = models.CharField(max_length=10)

    def __str__(self):
        return str(self.c_id)
class StatusClass():

    def status_200(self,data):
        responseJson = {}
        responseJson["status"] = 200
        responseJson["data"] = data
        responseJson["success"] = True
        return responseJson

    def status_400(self,data):
        responseJson = {}
        responseJson["status"] = 400
        responseJson["error"] = data
        responseJson["success"] = False
        return responseJson

    def status_201(self,data):
        responseJson = {}
        responseJson["status"] = 201
        responseJson["data"] = data
        responseJson["success"] = True
        return responseJson
    
    def status_other(self,message,status_code):
        responseJson = {}
        responseJson["status"] = status_code
        responseJson["error"] = message
        responseJson["success"] = False
        return responseJson
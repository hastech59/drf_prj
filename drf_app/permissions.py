# permissions.py
from rest_framework import permissions


class CustomAuthentication(permissions.BasePermission):

    edit_methods = ("PUT","GET","POST", "PATCH")

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return True

    def has_object_permission(self, request, view, obj):
        import pdb;pdb.set_trace()
        if request.user.is_superuser:
            return True

        if request.method in permissions.SAFE_METHODS:
            return True

        if obj.author == request.user:
            return True

        if request.user.is_staff and request.method not in self.edit_methods:
            return True

        return False
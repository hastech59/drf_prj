from django.contrib import admin
from drf_app.models import *

# Register your models here.
admin.site.register(UsersModal)
admin.site.register(AdsModal)
admin.site.register(CarsModal)
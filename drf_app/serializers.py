from rest_framework import serializers
from drf_app.models import *

class UserSerializer(serializers.Serializer):
    f_name = serializers.CharField(max_length=200)
    l_name = serializers.CharField(max_length=200)
    age = serializers.IntegerField()
    street = serializers.CharField(max_length=100)
    pin_code = serializers.CharField(max_length=10)
    city = serializers.CharField(max_length=100)
    state = serializers.CharField(max_length=100)
    country = serializers.CharField(max_length=100)
    username = serializers.CharField(max_length=20)
    password = serializers.CharField(max_length=20)

    class Meta:
        model = UsersModal
        fields = "__all__"

    def create(self,validated_data):
        return UsersModal.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.f_name = validated_data.get('f_name', instance.f_name)
        instance.l_name = validated_data.get('l_name', instance.l_name)
        instance.age = validated_data.get('age', instance.age)
        instance.street = validated_data.get('street', instance.street)
        instance.pin_code = validated_data.get('pin_code', instance.pin_code)
        instance.city = validated_data.get('city', instance.city)
        instance.state = validated_data.get('state', instance.state)
        instance.country = validated_data.get('country', instance.country)
        instance.username = validated_data.get('username', instance.username)
        instance.username = validated_data.get('username', instance.username)
        instance.save()
        return instance

    def validate_username(self,value):
        userCount = UsersModal.objects.filter(username=value).count()
        if userCount > 0:
            raise serializers.ValidationError("Username is already in used.")
        print("userCount",userCount)
        return value


class AdsSerializers(serializers.Serializer):
    title = serializers.CharField(max_length=100)
    description = serializers.CharField(max_length=200)
    price_per_km = serializers.CharField(max_length=10)

    def create(self,validated_data):
        validated_data["u_id"] = self.context['request'].user
        return AdsModal.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.description)
        instance.price_per_km = validated_data.get('price_per_km', instance.price_per_km)
        instance.save()
        return instance

    class Meta:
        model = AdsModal
        fields = "__all__"

class CarsSerializers(serializers.Serializer):
    c_id = serializers.IntegerField(required=False)
    brand = serializers.CharField(max_length=100)
    model = serializers.CharField(max_length=200)
    number_plate = serializers.CharField(max_length=10)

    def create(self,validated_data):
        validated_data["u_id"] = self.context['request'].user
        return CarsModal.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.brand = validated_data.get('brand', instance.brand)
        instance.model = validated_data.get('model', instance.model)
        instance.number_plate = validated_data.get('number_plate', instance.number_plate)
        instance.save()
        return instance

    class Meta:
        model = CarsModal
        fields = "__all__"
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import *
from .serializers import *
from drf_app.status import StatusClass
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.authentication import JWTAuthentication
from .permissions import CustomAuthentication
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token

class SignupView(APIView):

    def post(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            print(serializer.data)
            return Response(StatusClass.status_201(self,serializer.data),status=status.HTTP_201_CREATED)
        return Response(StatusClass.status_400(self,serializer.errors),status=status.HTTP_400_BAD_REQUEST)

class LoginView(APIView):

    def post(self, request, *args, **kwargs):
        
        userData = UsersModal.objects.get(username=request.data["username"],password=request.data["password"])
        token = get_tokens_for_user(userData)
        serializer = UserSerializer(userData).data
        serializer["token"] = token["access"]
        
        if userData:
            return Response(StatusClass.status_200(self,serializer),status=status.HTTP_200_OK)
        return Response(StatusClass.status_400(self,serializer.errors),status=status.HTTP_400_BAD_REQUEST)

class userView(APIView):
    # authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        users = UsersModal.objects.all()
        serializer = UserSerializer(users,many=True)
        return Response(StatusClass.status_200(self,serializer.data),status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            print(serializer.data)
            return Response(StatusClass.status_201(self,serializer.data),status=status.HTTP_201_CREATED)
        return Response(StatusClass.status_400(self,serializer.errors),status=status.HTTP_400_BAD_REQUEST)


    def put(self, request, u_id, *args, **kwargs):
        instance = UserModal.objects.get(u_id=u_id)
        data = request.data
        serializer = UserSerializers(instance=instance, data=data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(StatusClass.status_200(self,serializer.data),status=status.HTTP_200_OK)
        return Response(StatusClass.status_400(self,serializer.errors),status=status.HTTP_400_BAD_REQUEST)

class adsView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        ads = AdsModal.objects.all()
        serializer = AdsSerializers(ads,many=True)
        return Response(StatusClass.status_200(self,serializer.data),status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = AdsSerializers(data=data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(StatusClass.status_200(self,serializer.data),status=status.HTTP_200_OK)
        return Response(StatusClass.status_400(self,serializer.errors),status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, a_id, *args, **kwargs):
        instance = AdsModal.objects.get(a_id=a_id)
        data = request.data
        serializer = AdsSerializers(instance=instance, data=data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(StatusClass.status_200(self,serializer.data),status=status.HTTP_200_OK)
        return Response(StatusClass.status_400(self,serializer.errors),status=status.HTTP_400_BAD_REQUEST)

class carsView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        Cars = CarsModal.objects.all()
        serializer = CarsSerializers(Cars,many=True)
        return Response(StatusClass.status_200(self,serializer.data),status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = CarsSerializers(data=data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(StatusClass.status_200(self,serializer.data),status=status.HTTP_200_OK)
        return Response(StatusClass.status_400(self,serializer.errors),status=status.HTTP_400_BAD_REQUEST)
    
    
    def put(self, request, c_id, *args, **kwargs):
        instance = CarsModal.objects.get(c_id=c_id)
        data = request.data
        serializer = CarsSerializers(instance=instance, data=data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(StatusClass.status_200(self,serializer.data),status=status.HTTP_200_OK)
        return Response(StatusClass.status_400(self,serializer.errors),status=status.HTTP_400_BAD_REQUEST)

# class LogoutView(APIView):
#     permission_classes = [IsAuthenticated]

#     def post(self, request, *args,**kwargs):

        # refToken = request.data["refToken"]
        # refToken = RefreshToken(refToken)
        # refToken.blacklist()
        # return Response(StatusClass.status_200(self,serializer.data),status=status.HTTP_200_OK)

def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }
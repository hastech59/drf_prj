from django.contrib.auth.base_user import BaseUserManager

class MyUserManager(BaseUserManager):
    def create_user(u_id, username, password, f_name, zipcode, age, street, pin_code, city, country,state):

        user=self.model(
            u_id = u_id,
            username = username,
            password = password,
            zipcode = zipcode,
            f_name = f_name,
            age=age, 
            street = street,
            pin_code = pin_code,
            city = city,
            country = country,
            state = state
            )
        user.is_superuser = False
        user.is_admin = False
        user.is_staff = False
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, password, **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(username, password, **extra_fields)
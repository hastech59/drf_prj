from .views import *
from django.urls import path, include

urlpatterns = [
    path('userView/', userView.as_view(),name="userView"),
    path('userView/<int:u_id>/', userView.as_view(),name="userView"),
    path('adsView/', adsView.as_view(),name="adsView"),
    path('adsView/<int:a_id>/', adsView.as_view(),name="adsView"),
    path('carsView/', carsView.as_view(),name="carsView"),
    path('carsView/<int:c_id>/', carsView.as_view(),name="carsView"),
    path('signup/', SignupView.as_view(),name="SignupView"),
    path('login/', LoginView.as_view(),name="LoginView"),
    # path('logout/', LogoutView.as_view(),name="Logout"),
]
